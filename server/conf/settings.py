"""
Evennia settings file.

The available options are found in the default settings file found
here:

/home/ubuntu/vantamoo-py/lib/python3.10/site-packages/evennia/settings_default.py

Remember:

Don't copy more from the default file than you actually intend to
change; this will make sure that you don't overload upstream updates
unnecessarily.

When changing a setting requiring a file system path (like
path/to/actual/file.py), use GAME_DIR and EVENNIA_DIR to reference
your game folder and the Evennia library folders respectively. Python
paths (path.to.module) should be given relative to the game's root
folder (typeclasses.foo) whereas paths within the Evennia library
needs to be given explicitly (evennia.foo).

If you want to share your game dir, including its settings, you can
put secret game- or server-specific settings in secret_settings.py.

"""

# Use the defaults from Evennia unless explicitly overridden
from evennia.settings_default import *

from evennia.contrib.base_systems import color_markups

######################################################################
# Evennia base server config
######################################################################

# This is the name of your game. Make it catchy!
SERVERNAME = "vantaMOO"
GAME_SLOGAN = "vantaMOO!"
TELNET_PORTS = [4201]
INPUT_CLEANUP_BYPASS_PERMISSIONS = ["Builder"]
INPUT_CLEANUP_BYPASS_PERMISSIONS = ["Superuser"]
INPUT_CLEANUP_BYPASS_PERMISSIONS = ["Developer"]  
WEBCLIENT_ENABLED = True
WEBSOCKET_CLIENT_ENABLED = True

CSRF_TRUSTED_ORIGINS = ['https://minetest.vantaa.black', 'https://moo.vantaa.black' ]

# Required. This is a list of tuples
# (outgoing_port, internal_port). Only the outgoing
# port should be open to the world!
# set outgoing port to 80 if you want to run Evennia
# as the only web server on your machine (if available).
WEBSERVER_PORTS = [(4001, 4005)]

WEBSERVER_INTERFACES = ["192.9.248.110"]

# Required. Change this to the main IP address of your server.
WEBSOCKET_CLIENT_INTERFACE = "ws://192.9.248.110:4005"
# Optional and needed only if using a proxy or similar. Change
# to the IP or address where the client can reach
# your server. The ws:// part is then required. If not given, the client
# will use its host location.
# See above for the section on Lockdown Mode.
# Useful for a proxy on the public interface connecting to Evennia on localhost.
#LOCKDOWN_MODE = True
# nah not yet
WEBSOCKET_CLIENT_URL =  "wss://192.9.248.110:4002"
# Required. Change to a free port for the websocket client to reach
# the server on. This will be automatically appended
# to WEBSOCKET_CLIENT_URL by the web client.
WEBSOCKET_CLIENT_PORT = 4002

AMP_PORT = 4006

GUEST_ENABLED = True

######################################################################
# Settings given in secret_settings.py override those in this file.
######################################################################
try:
    from server.conf.secret_settings import *
except ImportError:
    print("secret_settings.py file not found or failed to import.")

# color_markups
COLOR_ANSI_EXTRA_MAP = color_markups.CURLY_COLOR_ANSI_EXTRA_MAP + \
    color_markups.MUX_COLOR_ANSI_EXTRA_MAP
COLOR_XTERM256_EXTRA_FG = color_markups.CURLY_COLOR_XTERM256_EXTRA_FG + \
    color_markups.MUX_COLOR_XTERM256_EXTRA_FG
COLOR_XTERM256_EXTRA_BG = color_markups.CURLY_COLOR_XTERM256_EXTRA_BG + \
    color_markups.MUX_COLOR_XTERM256_EXTRA_BG
COLOR_XTERM256_EXTRA_GFG = color_markups.CURLY_COLOR_XTERM256_EXTRA_GFG + \
    color_markups.MUX_COLOR_XTERM256_EXTRA_GFG
COLOR_XTERM256_EXTRA_GBG = color_markups.CURLY_COLOR_XTERM256_EXTRA_GBG + \
    color_markups.MUX_COLOR_XTERM256_EXTRA_GBG
COLOR_ANSI_XTERM256_BRIGHT_BG_EXTRA_MAP = color_markups.CURLY_COLOR_ANSI_XTERM256_BRIGHT_BG_EXTRA_MAP + \
    color_markups.MUX_COLOR_ANSI_XTERM256_BRIGHT_BG_EXTRA_MAP
