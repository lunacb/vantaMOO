"""
Room

Rooms are simple containers that has no location of their own.

"""
from evennia.contrib.rpg.rpsystem import ContribRPRoom

# rpsystem
class Room(ContribRPRoom):
    pass
